#!/usr/bin/env python
'''
A service that receives outgoing email by consuming from a queue and
sends out the email via an API (thoughts on using traditional SMTP as
a fallback sending method)
'''
import sys
import time
from lib.daemon_maker import Daemon
from lib.KeepUp import CONFIG_READER
from lib.KeepUp import Monitor
import traceback
import ConfigParser

APP_SETTINGS = dict(CONFIG_READER.items('app'))
PID_FILE = APP_SETTINGS['pid_file']

class Service(Daemon):
    '''
    The daemon class that runs the app as a UNIX service
    '''
    def run(self):
        '''
        Overridden method that runs the main method of the application
        It is rigged to work as an endless loop
        '''
        while True:
            monitor = Monitor()
            monitor.fix_service()
            time.sleep(10)

if __name__ == "__main__":
    DAEMON = Service(PID_FILE)
    if len(sys.argv) == 2:
        if "start" == sys.argv[1]:
            DAEMON.start()
        elif "stop" == sys.argv[1]:
            DAEMON.stop()
        elif "restart" == sys.argv[1]:
            DAEMON.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
