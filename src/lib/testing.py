'''
Test the service monitor main module
'''

from Monitor import Monitor

MONITOR = Monitor()

if __name__ == "__main__":
    MONITOR.fix_service()
