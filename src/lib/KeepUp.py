#! /usr/bin/env python
'''
Insert module doc-string here
'''

import mandrill
import logging
import logging.config
import ConfigParser
from subprocess import call

#CONFIG_FILE = '/home/leonard/Sandbox/services_monitor/src/settings.ini'
CONFIG_FILE = '/usr/local/lib/keepy_uppy/src/settings.ini'
CONFIG_READER = ConfigParser.ConfigParser()
CONFIG_READER.read(CONFIG_FILE)
MANDRILL_SETTINGS = dict(CONFIG_READER.items('mandrill'))
MONITOR_SETTINGS = dict(CONFIG_READER.items('monitor'))
logging.config.fileConfig(CONFIG_FILE)

class Monitor(object):
    '''
    Monitor services and restart them if necessary
    '''
    def __init__(self):
        self.pid_location = MONITOR_SETTINGS['pid_location']

    def check_pid_file(self):
        '''
        Check if the PID files are existing
        '''
        pid = None
        try:
            pid_file = open(self.pid_location, 'r')
            pid = pid_file.read()
            pid_file.close()
        except IOError:
            logging.info('Failed to find a PID - initiating restart')
        return pid

    def fix_service(self):
        '''
        Check if the service is up and running, if not, restart
        '''
        pid = self.check_pid_file()
        service_command = MONITOR_SETTINGS['start_command']
        if pid == None:
            #run the command to start the app and get the feedback
            call(service_command.split())
            #send email with the feedback
            message = '''The following command was run:
		                 \n%s\nIt had stopped''' % service_command
            mailer = Email(MANDRILL_SETTINGS['to'],
                           MANDRILL_SETTINGS['subject'],
                           message)
            mailer.send_via_mandrill()
            logging.info(message)
        else:
            logging.info('System is alive - waiting again')

class Email(object):
    '''
    Mailer system to send mail via mandrill or traditional mail (gmail)
    '''
    def __init__(self, to, subject, message):
        self.sender = MANDRILL_SETTINGS['from_email']
        self.sender_name = MANDRILL_SETTINGS['from_name']
        self.receiver = to
        self.mandrill_apikey = MANDRILL_SETTINGS['api_key']
        self.message = message
        self.subject = subject

    def send_via_mandrill(self):
        '''
        Enables connection to mandrill to send messages and fallback
        to SMTP on error
        '''
        try:
            client = mandrill.Mandrill(apikey=self.mandrill_apikey, debug=True)
            message = {'from_email':self.sender,
                       'subject': self.subject,
                       'from_name':self.sender_name,
                       'to':[{'email':self.receiver,
                              'name':'',
                              'type':'to'}],
                       'text':self.message
                      }
            client.messages.send(message)
        except Exception:
            raise

if __name__ == '__main__':
    monitor = Monitor()
    monitor.fix_service()
